__author__ = "Danilo S. Carvalho <danilo.carvalho@ppgi.ufrj.br>"

from setuptools import setup

setup(
    name='twitter_fetch',
    version='1.0.0',
    packages=["twitter_fetch"],
    url='',
    license='',
    author='Danilo S. Carvalho',
    author_email='danilo.carvalho@ppgi.ufrj.br',
    description='',
    install_requires=['pymongo', 'jsonlines', 'psycopg2-binary']
)
