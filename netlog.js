"use strict";
var page = require('webpage').create(),
    system = require('system'),
    address;
var fs = require('fs');
page.settings.userAgent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.14; rv:74.0) Gecko/20100101 Firefox/74.0';

if (system.args.length === 1) {
    console.log('Usage: netlog.js <some URL>');
    phantom.exit(1);
} else {
    address = system.args[1];

    page.onResourceRequested = function (req) {
        fs.write('/tmp/twitter_log.txt', JSON.stringify(req, undefined, 4), 'a');
    };

    page.open(address, function (status) {
        if (status !== 'success') {
            console.log('FAIL to load the address');
        }
        window.setTimeout(function() {
            phantom.exit();
        }, 10000);
    });
}
