__author__ = "Danilo S. Carvalho <danilo.carvalho@ppgi.ufrj.br>"

from twitter_fetch.fetcher import fetch_loop

langs = ["pt", "en"]
keywords = '(corona OR COVID OR virus OR vírus OR quarentena OR fiqueemcasa OR ficaemcasa OR wuhan OR pneumonia)'

since = "2020-04-01"
until = "2020-04-29"

for lang in langs:
    query = '%s since:%s until:%s' % (keywords, since, until)
    fetch_loop(query, lang=lang, persistence="csv", output_path="/tmp/", output_prefix="twtout")
