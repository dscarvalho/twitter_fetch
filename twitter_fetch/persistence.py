__author__ = "Danilo S. Carvalho <danilo.carvalho@ppgi.ufrj.br>"

import sys
import time
import jsonlines
import pymongo
import psycopg2
import csv
from datetime import datetime
from pymongo import MongoClient
from psycopg2 import IntegrityError, DatabaseError, OperationalError
from psycopg2.extras import DictCursor
from csv import DictWriter

DB = "twitter"
DB_HOST = "127.0.0.1"
DB_PORT = 27017
COLL_TWEETS = "tweets"
COLL_USERS = "users"


class MongoDB:
    def __init__(self):
        self.conn = MongoClient(host=DB_HOST, port=DB_PORT)
        self.db = self.conn[DB]
        self.coll_tweets = self.db[COLL_TWEETS]
        self.coll_users = self.db[COLL_USERS]
        self.coll_tweets.create_index("id", unique=True)
        self.coll_users.create_index("id", unique=True)

    def write_data(self, tweets, users):
        try:
            self.coll_tweets.insert_many([tweet.__dict__ for tweet in tweets.values()], ordered=False)
        except(pymongo.errors.BulkWriteError):
            pass

        try:
            self.coll_users.insert_many([user.__dict__ for user in users.values()], ordered=False)
        except(pymongo.errors.BulkWriteError):
            pass


class JsonLines:
    def __init__(self, file_prefix):
        self.file_prefix = file_prefix

    def write_data(self, tweets, users):
        with jsonlines.open(self.file_prefix + "_tweets.jsonl", "a") as tweet_writer:
            for tweet in tweets.values():
                tweet_writer.write(tweet.__dict__)

        with jsonlines.open(self.file_prefix + "_users.jsonl", "a") as user_writer:
            for user in users.values():
                user_writer.write(user.__dict__)


class CSV:
    def __init__(self, file_prefix):
        self.file_prefix = file_prefix

    def write_data(self, tweets, users, write_header=False):
        with open(self.file_prefix + "_tweets.csv", "a") as tweet_file:
            writer = DictWriter(tweet_file,
                                ["id", "userid", "content", "timestamp_ms", "timestamp_ext", "mentions", "hashtags",
                                 "location", "language"],
                                quoting=csv.QUOTE_NONNUMERIC)
            if (write_header):
                writer.writeheader()

            for tweet in tweets.values():
                tweet_dic = dict(tweet.__dict__)
                tweet_dic["content"] = tweet.content.strip()
                tweet_dic["timestamp_ms"] = tweet.timestamp["ms"]
                tweet_dic["timestamp_ext"] = tweet.timestamp["ext"]
                tweet_dic["mentions"] = ";".join(tweet.mentions)
                tweet_dic["hashtags"] = ";".join(tweet.hashtags)
                tweet_dic["location"] = tweet.location
                tweet_dic["language"] = tweet.language
                del tweet_dic["timestamp"]

                writer.writerow(tweet_dic)

        with open(self.file_prefix + "_users.csv", "a") as user_file:
            writer = DictWriter(user_file, ["id", "name", "screenname"], quoting=csv.QUOTE_NONNUMERIC)

            if (write_header):
                writer.writeheader()

            for user in users.values():
                writer.writerow(user.__dict__)


class PostgreSQL:
    def __init__(self, connstring):
        self.connstring = connstring
        self.conn = psycopg2.connect(connstring)
        self.cur = self.conn.cursor(cursor_factory=DictCursor)

    def reconnect(self):
        try:
            self.conn.close()
        except:
            pass
        while (True):
            try:
                self.conn = psycopg2.connect(self.connstring)
                self.cur = self.conn.cursor()
                break
            except:
                print("Cannot connect do DB yet...")
                pass

    def write_data(self, tweets, users):
        for user in users.values():
            while (True):
                try:
                    self.cur.execute("""insert into users (user_id, user_name, screenname) values (%s, %s, %s)""",
                                     (user.id, user.name, user.screenname))
                    self.conn.commit()
                    break
                except IntegrityError:
                    self.conn.rollback()
                    self.conn.commit()
                    break
                except DatabaseError as e:
                    time.sleep(5)
                    print("DatabaseError", e.args)
                    self.reconnect()
                except OperationalError as e:
                    time.sleep(5)
                    print("OperationalError", e.args)
                    self.reconnect()

        for tweet in tweets.values():
            while (True):
                try:
                    try:
                        tweet_time = datetime.strptime(tweet.timestamp["ext"], "%I:%M %p - %d %b %Y")
                    except ValueError:
                        tweet_time = None

                    self.cur.execute("""insert into tweets (tweet_id, user_id, msg_content, timestamp_ext, 
                                                            mentions, hashtags, sent_location, msg_language) 
                                               values (%s, %s, %s, %s, %s, %s, %s, %s)""",
                                     (tweet.id, tweet.userid, tweet.content.strip(), tweet_time, ";".join(tweet.mentions),
                                      ";".join(tweet.hashtags), tweet.location, tweet.language))
                    self.conn.commit()
                    break
                except IntegrityError:
                    self.conn.rollback()
                    self.conn.commit()
                    break
                except DatabaseError as e:
                    time.sleep(5)
                    print("DatabaseError", e.args)
                    self.reconnect()
                except OperationalError as e:
                    time.sleep(5)
                    print("OperationalError", e.args)
                    self.reconnect()
