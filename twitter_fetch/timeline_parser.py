#-*- coding: utf-8 -*-
__author__ = "Danilo S. Carvalho <danilo.carvalho@ppgi.ufrj.br>"

import re
from html.parser import HTMLParser

from .html_stripper import strip_tags
from .data_model import Tweet, User



class TimelineParser(HTMLParser):
    def __init__(self):
        super().__init__()
        self.curId = ""
        self.tweets = dict()
        self.users = dict()
        self.oncontent = False

    def handle_starttag(self, tag, attrs):
        attrs = dict(attrs)
        if (tag == "li" and "data-item-id" in attrs and attrs["data-item-type"] == "tweet"):
            tweet = Tweet()
            self.curId = attrs["data-item-id"]
            tweet.id = self.curId
            self.tweets[tweet.id] = tweet

        if (tag == "div" and "data-tweet-id" in attrs and attrs["data-tweet-id"] == self.curId):
            if (attrs["data-user-id"] not in self.users):
                user = User()
                user.id = attrs["data-user-id"]
                user.screenname = attrs["data-name"]
                user.name = attrs["data-screen-name"]
                self.users[user.id] = user
                self.tweets[self.curId].userid = user.id


        if (tag == "div" and "data-mentions" in attrs and attrs["data-tweet-id"] == self.curId):
            self.tweets[self.curId].mentions = attrs["data-mentions"].split()

        if (tag == "a" and "class" in attrs and "tweet-timestamp" in attrs["class"] and self.curId):
            self.tweets[self.curId].timestamp["ext"] = attrs["title"]

        if (tag == "span" and "class" in attrs and "_timestamp" in attrs["class"] and self.curId):
            self.tweets[self.curId].timestamp["ms"] = int(attrs["data-time-ms"])

        if (tag == "div" and attrs["class"] == "js-tweet-text-container"):
            self.oncontent = True


    def handle_endtag(self, tag):
        if (tag == "div" and self.oncontent):
            self.oncontent = False
            content_words = self.tweets[self.curId].content.split()
            hashtags = set()
            for word in content_words:
                if (re.match(r"^#.+", word)):
                    hashtags.add(word)

            self.tweets[self.curId].hashtags = list(hashtags)

    def handle_data(self, content):
        if (self.oncontent):
            self.tweets[self.curId].content += strip_tags(content)
