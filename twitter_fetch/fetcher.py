#!/usr/bin/env python3
#-*- coding: utf-8 -*-
__author__ = "Danilo S. Carvalho <danilo.carvalho@ppgi.ufrj.br>"

import sys
import os
import time
import random
import urllib.request
import urllib.parse
from urllib.error import HTTPError, URLError
import json

from .persistence import MongoDB, JsonLines, CSV, PostgreSQL
from .timeline_parser import TimelineParser

URL = "https://twitter.com/i/search/timeline?"
INTERVAL_FACTOR = 1


def main(argv):
    fetch_loop(argv[1], argv[2], int(argv[3]))


def fetch_loop(query: str, start: str = "", num_iterations: int = 0, lang: str = "", location: str = "",
               persistence: str = "csv", output_path: str = "", output_prefix: str = "", connstring: str = ""):
    tweets, users, seq = fetch(query, start, lang, location)

    if (persistence == "jsonlines"):
        db = JsonLines(os.path.join(output_path, output_prefix))
    elif (persistence == "csv"):
        db = CSV(os.path.join(output_path, output_prefix))
    elif (persistence == "mongodb"):
        db = MongoDB(connstring)
    elif (persistence == "postgresql"):
        db = PostgreSQL(connstring)

    if (persistence == "csv"):
        db.write_data(tweets, users, True)
    else:
        db.write_data(tweets, users)
    #print_data(tweets, users)

    i = 0
    while (num_iterations == 0 or i <= num_iterations):
        try:
            tweets, users, seq = fetch(query, seq, lang, location)
        except(HTTPError):
            print("Fetch failed (HTTPError): retrying...")
            time.sleep(5)
            continue
        except(ConnectionResetError):
            print("Fetch failed (ConnectionResetError): retrying...")
            time.sleep(5)
            continue
        except Exception as e:
            print("Fetch failed (Unspecified Error): retrying...", e.args)
            time.sleep(5)
            continue

        if (len(tweets) > 0):
            db.write_data(tweets, users)
            #print_data(tweets, users)
            print("Written %d tweets from %s in %s..." % (len(tweets), location, lang))
        else:
            print("End.")
            break

        time.sleep(random.random() * INTERVAL_FACTOR)
        i += 1




def fetch(query:str, seq:str, lang:str, loc:str=""):
    params = dict()
    params["vertical"] = "default"
    params["l"] = lang
    params["q"] = query
    params["f"] = "tweets"
    params["src"] = "typd"
    params["include_available_features"] = "1" 
    params["include_entities"] = "1"
    params["max_position"] = seq
    params["reset_error_state"] = "false"

    url_params = urllib.parse.urlencode(params, quote_via=urllib.parse.quote)

    headers = {
            "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.13; rv:61.0) Gecko/20100101 Firefox/61.0",
            "Host": "twitter.com",
            #"Cookie": 'personalization_id="v1_FWHO+Zd7Gpcs81bUrqfxSw=="; guest_id=v1%3A152979413048975558; syndication_guest_id=v1%3A153140480494153963; tfw_exp=0; _twitter_sess=BAh7CSIKZmxhc2hJQzonQWN0aW9uQ29udHJvbGxlcjo6Rmxhc2g6OkZsYXNo%250ASGFzaHsABjoKQHVzZWR7ADoPY3JlYXRlZF9hdGwrCMQ%252BDDllAToMY3NyZl9p%250AZCIlZWY4OGQ3ODA2MmQwY2I4YzYxNzhlNGY5NTc1NzVmZmI6B2lkIiVlZGVh%250AYTlhYmYyYTFhZTAyNGI3MGQ3N2Q2M2NlNGQ3NA%253D%253D--87bfa208e2ceb72e192625cf4c4d98ff7f435e91; ct0=0828b7c991f3cc8d05f2426201e72e20; lang=en; gt=1029401971931983872',
            "Accept-language": "en-US,en;q=0.5",
            #"Accept-encoding": "gzip, deflate, br",
            "Accept": "application/json, text/javascript, */*; q=0.01",
            "X-Requested-With": "XMLHttpRequest",
            "X-Twitter-Active-User": "yes",
            "Connection": "keep-alive",
            "Pragma": "no-cache",
            "Cache-Control": "no-cache"
            }

    print("URL: ", URL + url_params)
    req = urllib.request.Request(URL + url_params, None, headers)

    while (True):
        try:
            with urllib.request.urlopen(req) as response:
                res = json.loads(response.read().decode("utf-8"))
            break
        except Exception as e:
            print("Request failed: ",  e.args)
            time.sleep(5)


    tweet_html = res["items_html"]
    parser = TimelineParser()

    parser.feed(tweet_html)

    for tweet in parser.tweets:
        parser.tweets[tweet].location = loc
        parser.tweets[tweet].language = lang

    return (parser.tweets, parser.users, res["min_position"])





def print_data(tweets, users):
    for tweet in tweets.values():
        print(tweet.__dict__)

    #for user in users.values():
    #    print(user.__dict__)



if __name__ == "__main__":
    main(sys.argv)
