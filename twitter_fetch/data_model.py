__author__ = "Danilo S. Carvalho <danilo.carvalho@ppgi.ufrj.br>"

class Tweet:
    def __init__(self):
        self.id = ""
        self.userid = ""
        self.content = ""
        self.timestamp = {"ms": 0, "ext": ""}
        self.mentions = []
        self.hashtags = []
        self.location = ""
        self.language = ""


class User:
    def __init__(self):
        self.id = ""
        self.name = ""
        self.screenname = ""