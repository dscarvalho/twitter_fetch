#!/bin/bash

counter=0
while :
do
    phantomjs ./netlog.js https://twitter.com/search?q=bolsonaro&src=typed_query
    sleep 10s
    grep '"name": "x-guest-token"' -A1 /tmp/twitter_log.txt | tail -n 1 | awk '/value/' | grep -o '[0-9]\+' >> gtokens.txt
    rm /tmp/twitter_log.txt
    sleep 110s
    counter=$(($counter + 1))

    if [ $(($counter % 30)) == 0 ]; then
        tail -n 20 gtokens.txt > /tmp/gtokens_buffer.txt
        mv /tmp/gtokens_buffer.txt ./gtokens.txt
    fi
done
